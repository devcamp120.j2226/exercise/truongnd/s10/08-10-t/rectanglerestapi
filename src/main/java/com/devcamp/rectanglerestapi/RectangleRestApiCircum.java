package com.devcamp.rectanglerestapi;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleRestApiCircum {
    @CrossOrigin
    @GetMapping("/rectangle-perimeter")
    public ArrayList<Rectangle> getListRectangleItem(){
        Rectangle hinhchunhat3 = new Rectangle();
        Rectangle hinhchunhat4 = new Rectangle(4.0f,5.0f);

        System.out.println(hinhchunhat3.toString());
        System.out.println(hinhchunhat4.toString());
        System.out.println(hinhchunhat3.getPerimeter());
        System.out.println(hinhchunhat4.getArea());


        ArrayList<Rectangle> rectangleItems = new ArrayList<>();
        rectangleItems.add(hinhchunhat3);
        rectangleItems.add(hinhchunhat4);

        return rectangleItems;
    }
}

