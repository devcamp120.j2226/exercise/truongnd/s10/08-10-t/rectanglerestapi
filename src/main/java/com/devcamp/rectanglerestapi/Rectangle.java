package com.devcamp.rectanglerestapi;

public class Rectangle {
    protected float length = 1.0f;
    protected float width = 1.0f;

    public Rectangle(){

    }

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }
    public double getPerimeter(){
        return (this.width+this.length)*2;
    }
    public double getArea(){
        return this.length*this.width;
    }
    @Override
    public String toString(){
        return "Rectangle[length= " + this.length + " ,width= " + this.width+ "]";
    }
}
