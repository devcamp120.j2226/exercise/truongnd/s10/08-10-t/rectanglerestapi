package com.devcamp.rectanglerestapi;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleRestApiArea {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public ArrayList<Rectangle> getListRectangleItem(){
        Rectangle hinhchunhat1 = new Rectangle();
        Rectangle hinhchunhat2 = new Rectangle(2.0f,3.0f);

        System.out.println(hinhchunhat1.toString());
        System.out.println(hinhchunhat2.toString());
        System.out.println(hinhchunhat1.getArea());
        System.out.println(hinhchunhat2.getArea());


        ArrayList<Rectangle> rectangleItems = new ArrayList<>();
        rectangleItems.add(hinhchunhat1);
        rectangleItems.add(hinhchunhat2);

        return rectangleItems;
    }
}
